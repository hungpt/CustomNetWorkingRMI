
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author THANH HUNG
 */
public interface IEmployeeMng extends Remote {

    Vector getInitData() throws RemoteException;
    boolean saveList(Vector data) throws RemoteException;
}
